package com.example.tablelayout2recyclerview;

import android.content.res.TypedArray;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

public class RowDescriptor {

    private static final String TAG = "RowDescriptor";

    private final String name;
    private final TypedArray typedArray;
    private final RowViewHolder.Factory factory;

    @NonNull
    private final Map<String, Object> values = new HashMap<>();

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public RowViewHolder.Factory getFactory() {
        return factory;
    }

    @NonNull
    public TypedArray getTypedArray() {
        return typedArray;
    }

    public RowDescriptor(String name, TypedArray typedArray) {
        this.name = name;
        this.typedArray = typedArray;
        switch (name) {
            case "CompoundSwitch":
                this.factory = SwitchViewHolder::new;
                break;
            case "CompoundEditText":
                this.factory = EditTextViewHolder::new;
                break;
            default:
                this.factory = null;
        }
    }

    int getId() {
        String id = getValue("id");
        return id != null ? Integer.parseInt(id.replace("@", "")) : 0;
    }

    int getInputType() {
        String inputType = getValue("inputType");
        return inputType != null ? Integer.decode(inputType) : 0;
    }

    @Nullable
    @SuppressWarnings("unchecked")
    // generic return type: https://stackoverflow.com/a/450852/4070848
    public <T> T getValue(@NonNull String key) {
        T value = (T) values.get(key);
        Log.d(TAG, "getValue " + key + ", " + value);
        return value;
    }

    @NonNull
    public <T> RowDescriptor putValue(@NonNull String key, @Nullable T value) {
        Log.d(TAG, "putValue " + key + ", " + value);
        values.put(key, value);
        return this;
    }
}
