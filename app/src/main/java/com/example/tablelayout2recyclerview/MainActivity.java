package com.example.tablelayout2recyclerview;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.XmlRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// this project is based on a combination of the approach suggested by:
//   https://stackoverflow.com/a/67820259/4070848
// and the approach suggested by:
//   https://stackoverflow.com/a/48123513/4070848
// the former provides a neat way of creating and binding view holders for different view types
// while the latter provides a neat way to retain use of compound controls, so that in onCreateViewHolder()
// we instantiate the relevant compound control (which itself handles inflation) rather than inflating
// the appropriate layout directly
// this was all in response to my question here: https://stackoverflow.com/q/67737033/4070848

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    // using XML files to specify the custom controls for each section is historical,
    // since the custom controls were originally included directly in the layout XML for each section
    // migration plan is eventually to do away with the XML files and just pass the attributes in directly
    private static final String[] names = { "CompoundSwitch", "CompoundEditText" };
    private static final List<String> namesList = Arrays.asList(names);

    @Override
    protected void onCreate(@Nullable Bundle state) {
        super.onCreate(state);

        setContentView(R.layout.activity_main);

        List<RowDescriptor> rows = new ArrayList<>();

        addRows(rows, R.xml.section_a);
        addRows(rows, R.xml.section_b);

        RowsAdapter adapter = new RowsAdapter(rows);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
    }

    private void addRows(List<RowDescriptor> rows, @XmlRes int id) {
        // extract attributes from XML file... based on:
        // https://stackoverflow.com/a/38690715/4070848

        XmlPullParser parser = getResources().getXml(id);

        while (true) {
            try {
                int eventType = parser.getEventType();
                Log.d(TAG, "eventType: " + eventType);
                if (eventType == XmlPullParser.END_DOCUMENT) {
                    break;
                }
                // can only extract attributes when eventType is START_TAG: https://stackoverflow.com/q/67854444/4070848 ...
                if (eventType == XmlPullParser.START_TAG) {
                    String name = parser.getName();
                    name = name.replace(getPackageName() + ".", "");
                    Log.d(TAG, "name: " + name);
                    // and only extract attributes for the elements we are actually wanting to parse...
                    if (namesList.contains(name)) {
                        RowDescriptor row = extractAttributes(parser, name);
                        rows.add(row);
                    }
                }
                parser.next();

            } catch (IOException | XmlPullParserException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    @NonNull
    private RowDescriptor extractAttributes(XmlPullParser parser, @NonNull String name) {
        AttributeSet attrs = Xml.asAttributeSet(parser);
        // the above AttributeSet is NOT RETAINED because the parser will move on
        // to the next elements with parser.next() below
        // see https://stackoverflow.com/q/67857450/4070848
        // hence add them directly to the RowDescriptor in a different format...
        int[] styleable = {};
        switch (name) {
            case "CompoundSwitch":
                styleable = R.styleable.CompoundSwitch;
                break;
            case "CompoundEditText":
                styleable = R.styleable.CompoundEditText;
                break;
        }
        TypedArray typedArray = getTheme().obtainStyledAttributes(
                attrs,
                styleable,
                0, 0);
        // so we have extracted the attrs into a typedArray which we can load into the RowDescriptor
        // as a record of the attributes for this row...
        RowDescriptor row = new RowDescriptor(name, typedArray);
        // xxx BUT for some reason, the "android:id" isn't transferring across, even though e.g. "android:inputType" is
        // so we need (xxx for now) to write *at least* the "android:id" (which has a key of just "id") "manually"
        // into the RowDescriptor so that we can access it later... easiest just to write *all* attributes even though
        // we only use (at the moment) the id... in future (when not relying on XML) we would do something
        // like this anyway, i.e. writing key/value pairs for every attribute (since we won't have a TypedArray
        // holding all of the attributes)
        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            // xxx improve this in future so that we can put the attribute value with the right
            // variable type (String or int or boolean)... the following just adds them all as String...
            // the Map in RowDescriptor should already be capable of holding any variable type...
            row.putValue(attrs.getAttributeName(i), attrs.getAttributeValue(i));
        }
        return row;
    }
}
