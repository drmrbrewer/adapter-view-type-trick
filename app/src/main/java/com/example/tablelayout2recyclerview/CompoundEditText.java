package com.example.tablelayout2recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

// a compound control (custom view)...
// https://developer.android.com/guide/topics/ui/custom-components#compound
// https://developer.android.com/training/custom-views/create-view

// note that we extend from LinearLayout even though the inflated layout is a TableRow
// this is the only way I could get the column sizing to work as expected
// (also note that, in the Android code, TableRow itself extends LinearLayout)
public class CompoundEditText extends LinearLayout {

    private static final String TAG = "CompoundEditText";

    EditText editText;

    // This is the code constructor.  It can be called directly from code to create a new instance of the view.  This constructor doesn't have access to XML attributes, so you have to fill the parameters manually, using setters.
    public CompoundEditText(Context context) {
        super(context);
    }

    // This is the basic XML constructor.  The AttributeSet parameter contains all attribute values provided via XML.
    public CompoundEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    // all the properties we need to extract from attributes...
    static class Props {
        Integer inputType;
        Integer id;
        String labelTag;
        String labelText;
        String labelText2;
        Boolean isIndented;
    }

    Props getProps(TypedArray a) {
        try {
            Props props = new Props();
            // re-use standard attributes: https://stackoverflow.com/q/42662790/4070848
            props.inputType = a.getInt(R.styleable.CompoundEditText_android_inputType, 0);
            props.id = a.getInt(R.styleable.CompoundEditText_android_id, 0);
            // and now my custom attributes...
            props.labelTag = a.getString(R.styleable.CompoundEditText_edit_text_label_tag);
            props.labelText = a.getString(R.styleable.CompoundEditText_edit_text_label_text);
            props.labelText2 = a.getString(R.styleable.CompoundEditText_edit_text_label_text_2);
            props.isIndented = a.getBoolean(R.styleable.CompoundEditText_edit_text_indented, false);
            return props;
        } finally {
            a.recycle();
        }
    }

    // version where we get the required properties from the RowDescriptor
    // i.e. where we add the custom view programmatically to a RecyclerView
    public void init(Context context, RowDescriptor row) {
        TypedArray a = row.getTypedArray();
        Props props = getProps(a);
        // for some reason, the "android:id" isn't transferring across, even though e.g. "android:inputType" is
        // so read the id from the attribute values we also added "manually"...
        props.id = row.getId();
        init(context, props);
    }

    // version where we get the required properties from the AttributeSet
    // i.e. from XML constructor here we reference the custom view directly in the XML
    // xxx do we do this anymore?
    public void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CompoundEditText,
                0, 0);
        Props props = getProps(a);
        init(context, props);
    }

    public void init(Context context, Props props) {
        this.setId(props.id);
        if (props.labelText2 == null || props.labelText2.equals("")) {
            inflate(context, R.layout.compound_edit_text, this);
        } else {
            inflate(context, R.layout.compound_edit_text_2, this);
            TextView labelTextView2 = findViewById(R.id.edit_text_label_2);
            labelTextView2.setText(props.labelText2);
        }
        TextView textView = findViewById(R.id.edit_text_label);
        textView.setText(props.labelText);
        textView.setTag(props.labelTag); // for link to help page
        if (props.isIndented) {
            MyFunctions.setPaddingLeft(textView, 20/*dp*/);
        }
        editText = findViewById(R.id.edit_text_control);
        editText.setInputType(props.inputType);
    }

    @NonNull
    public EditText getControl() {
        return editText;
    }

    @NonNull
    public String getText() {
        return editText.getText().toString();
    }

    public void setText(String text) {
        editText.setText(text);
    }
}
