package com.example.tablelayout2recyclerview;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RowsAdapter extends RecyclerView.Adapter<RowViewHolder> {
    private static final String TAG = "RowsAdapter";

    @Nullable
    private final List<? extends RowDescriptor> rows;

    @NonNull
    private final Map<String, Integer> nameToViewType = new HashMap<>();

    @NonNull
    private final SparseArray<String> viewTypeToName = new SparseArray<>();

    @NonNull
    private final SparseArray<RowViewHolder.Factory> viewTypeToFactory = new SparseArray<>();

    public RowsAdapter(@Nullable List<RowDescriptor> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows != null ? rows.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if ( rows != null && isValid(position) ) {
            RowDescriptor row = rows.get(position);

            String name = row.getName();
            Integer viewType = nameToViewType.get(name);
            if (viewType == null || viewType == 0) {
                // keep mapping between name (String) and viewType (int)
                viewType = nameToViewType.size() + 1;
                nameToViewType.put(name, viewType);
                Log.d(TAG, "nameToViewType size is now: " + nameToViewType.size());
                // and also other mappings...
                viewTypeToName.put(viewType, name);
                viewTypeToFactory.put(viewType, row.getFactory());
            }

            return viewType;
        } else {
            return 0;
        }
    }

    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == 0) {
            return emptyViewHolder(context);
        }

        // typically here you would inflate the appropriate layout directly here
        // but instead we are creating the appropriate custom view, which does the inflation itself
        // this approach is based on: https://stackoverflow.com/a/48123513/4070848

        RowViewHolder.Factory factory = viewTypeToFactory.get(viewType);
        if (factory == null) {
            return emptyViewHolder(context);
        }
        String name = viewTypeToName.get(viewType);
        switch (name) {
            case "CompoundSwitch":
                CompoundSwitch compoundSwitch = new CompoundSwitch(context);
                return factory.newInstance(compoundSwitch);
            case "CompoundEditText":
                CompoundEditText compoundEditText = new CompoundEditText(context);
                return factory.newInstance(compoundEditText);
            default:
                return emptyViewHolder(context);
        }
    }

    private boolean isValid(int position) {
        return rows != null && position >= 0 && position < rows.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder holder, int position) {
        Context context = holder.itemView.getContext();
        RowDescriptor rowDescriptor = rows != null && isValid(position) ? rows.get(position) : null;
        holder.onBind(context, rowDescriptor);
        setupView(holder);
    }

    @NonNull
    RowViewHolder emptyViewHolder(Context context) {
        return new RowViewHolder(new View(context));
    }

    private void setupView(@NonNull RowViewHolder holder) {
        View view = holder.itemView;
        // to demonstrate that setting the ID of the programmatically-created
        // compound control "manually" based on the android:id specified in the
        // XML file actually works...
        view.postDelayed(() -> {
            CompoundSwitch compoundSwitch = view.findViewById(R.id.switching);
            if (compoundSwitch != null) {
                compoundSwitch.setChecked(false);
            }
        }, 2000);
    }
}
