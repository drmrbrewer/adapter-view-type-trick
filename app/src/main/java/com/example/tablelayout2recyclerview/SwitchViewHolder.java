package com.example.tablelayout2recyclerview;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SwitchViewHolder extends RowViewHolder {

    private final CompoundSwitch compoundSwitch;

    // this constructor is invoked in RowsAdapter.onCreateViewHolder() with a call to factory.newInstance(view), passing in the view for the inflated layout...
    public SwitchViewHolder(@NonNull View view) {
        super(view);
        this.compoundSwitch = (CompoundSwitch) view;
    }

    @Override
    public void onBind(Context context, @Nullable RowDescriptor row) {
        if (row != null) {
            // initialise the view based on the attributes extracted from XML...
            compoundSwitch.init(context, row);
        }
    }
}
