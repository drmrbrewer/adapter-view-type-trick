package com.example.tablelayout2recyclerview;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EditTextViewHolder extends RowViewHolder {

    private final CompoundEditText compoundEditText;

    // this constructor is invoked in RowsAdapter.onCreateViewHolder() with a call to factory.newInstance(view), passing in the view for the inflated layout...
    public EditTextViewHolder(@NonNull View view) {
        super(view);
        this.compoundEditText = (CompoundEditText) view;
    }

    @Override
    public void onBind(Context context, @Nullable RowDescriptor row) {
        if (row != null) {
            // initialise the view based on the attributes extracted from XML...
            compoundEditText.init(context, row);
        }
    }
}
