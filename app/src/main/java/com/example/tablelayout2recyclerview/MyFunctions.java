package com.example.tablelayout2recyclerview;

import android.content.res.Resources;
import android.view.View;

public class MyFunctions {
    public static void setPaddingLeft(View view, int leftPaddingDp) {
        int leftPaddingPx = dpToPx(leftPaddingDp);
        view.setPadding(leftPaddingPx, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
    }

    // https://stackoverflow.com/a/19953871/4070848
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
