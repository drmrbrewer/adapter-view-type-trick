package com.example.tablelayout2recyclerview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class RowViewHolder extends RecyclerView.ViewHolder {
    public RowViewHolder(@NonNull View itemView) {
        super(itemView);
        // found I needed to set the following LayoutParams on the itemView otherwise my
        // custom views (compound controls) don't extend across the full width of the parent
        // (even with match_parent in the XML)...
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        itemView.setLayoutParams(lp);
    }

    public void onBind(Context context, @Nullable RowDescriptor row) {
        // none
    }

    public interface Factory {
        @NonNull
        RowViewHolder newInstance(@NonNull View itemView);
    }
}
