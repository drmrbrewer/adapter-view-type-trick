# Adapter viewType trick

Example project for this [SO answer][1].

Based on the original project [here][2] by Oleksii K.

 [1]: https://stackoverflow.com/a/67820259/1891118
 [2]: https://github.com/ok3141/adapter-view-type-trick
 